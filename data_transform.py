import pandas as pd
import numpy as np
from collections import OrderedDict
import scipy.stats

def winsorize_series(series, limit):
    """
    scipy stats does not work with NaNs
    https://github.com/scipy/scipy/issues/8327
    """
    series = series.copy()
    series[~series.isna()] = scipy.stats.mstats.winsorize(series[~series.isna()], limit)
    return series

def normalize_exposures(exposures, members, winsorize_limit=0.01):
    winsorized = exposures.apply(winsorize_series, axis=1, args=(winsorize_limit,))
    
    means = winsorized.mean(axis=1, skipna=True)
    stds  = winsorized.std(axis=1, skipna=True)
    
    assert(np.isfinite(means).all())
    assert(np.isfinite(stds).all())
    assert((stds > 0).all())
    
    return winsorized.subtract(means, axis=0).divide(stds, axis=0)

def append_index_returns(asset_prices, index_prices):
    """
    after company goes bankrupt, reinvest in ETF
    """
    assert((asset_prices.index == index_prices.index).all())
    new_prices = asset_prices.copy()
    last_date = asset_prices.dropna().index[-1]
    new_prices.loc[last_date:] = asset_prices[last_date]*index_prices[last_date:]/index_prices[last_date]
    return new_prices

def prepend_index_returns(etf_prices, index_prices):
    """
    index tends to predate the ETF
    extends the ETF with index returns
    """
    assert((etf_prices.index == index_prices.index).all())
    new_prices = etf_prices.copy()
    first_date = etf_prices.dropna().index[0]
    new_prices.loc[:first_date] = etf_prices[first_date]*index_prices[:first_date]/index_prices[first_date]
    return new_prices

def restrict_universe_to_field(field_df, members):
    begin_date = max(field_df.index[0],  members.index[0])
    end_date   = min(field_df.index[-1], members.index[-1])
    
    joint_index = field_df.index.union(members.index)
    
    restricted_index = joint_index[(begin_date <= joint_index) & (joint_index <= end_date)]    
    prev_universe = frozenset()
    
    restricted_members = OrderedDict()

    for date in restricted_index:
        # find last membership update
        current_universe = list(members.iloc[members.index.get_loc(date, method='ffill')])
        # restrict to things that have a price
        current_universe = frozenset(field_df.iloc[field_df.index.get_loc(date, method='ffill')][current_universe].dropna().index)

        if prev_universe != current_universe:
            restricted_members[date] = current_universe

        prev_universe = current_universe
        
    
    return pd.Series(restricted_members)


def restrict_field_to_universe(field_df, members):
    begin_date = max(field_df.index[0],  members.index[0])
    end_date   = min(field_df.index[-1], members.index[-1])
    
    restricted_exposures = field_df.loc[begin_date:end_date].copy()

    for date in restricted_exposures.index:

        current_universe = members.iloc[members.index.get_loc(date, method='ffill')]
        restricted_exposures.loc[date, ~field_df.columns.isin(current_universe)] = np.nan
    
    # drop any remaining NA rows
    # is this really necessary?
    return restricted_exposures.dropna(how='all', axis=0)
