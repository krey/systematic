from builtins import range

import pandas as pd
import numpy as np
import scipy.stats
from collections import OrderedDict

def calculate_IC(exposures, forward_rets):
    """
    exposures: pd.DataFrame. Columns: tickers, rows: dates, values: factor exposures, restricted to universe
    forward_rets: pd.DataFrame, Columns: tickers, rows: dates, values: return over a future period
    """
    begin_date = max(exposures.index[0],  forward_rets.index[0])
    end_date   = min(exposures.index[-1], forward_rets.index[-1])
    trading_days = exposures.index[(begin_date <= exposures.index) & (exposures.index <= end_date)]
    ic = OrderedDict()

    for date in trading_days:
        current_exposures = exposures.loc[date].dropna()
        current_universe = current_exposures.index
        current_forward_returns = forward_rets.loc[date, current_universe]
        # returns should be extrapolated as appropriate before being passed in
        assert(current_forward_returns.index[current_forward_returns.isna()].empty)
        corr = scipy.stats.spearmanr(current_forward_returns, current_exposures).correlation
        
        ic[date] = corr
    
    return pd.Series(ic)

def quantile_portfolios(exposures, cuts=10):
    """
    exposures: pd.DataFrame. Columns: tickers, rows: dates, values: factor exposure, restricted to universe
    """
    gamma_portfolios = [pd.DataFrame(0., index=exposures.index, columns=exposures.columns) for _ in range(cuts)]

    for date in exposures.index:
        current_exposures = exposures.loc[date].dropna()
        current_universe = current_exposures.index
        
        cut_indices = np.round(np.linspace(0, len(current_universe), cuts+1)).astype(int)
        sorted_universe = current_exposures.sort_values().index
        for i, ix in enumerate(zip(cut_indices[:-1], cut_indices[1:])):
            gamma_universe = sorted_universe[ix[0]:ix[1]]
            gamma_portfolios[i].loc[date, gamma_universe] = 1./len(gamma_universe)
    
    return gamma_portfolios

def portfolio_returns(portfolio_weights, rets_1day):
    """
    portfolio_weights: DataFrame. Columns: tickers, rows: dates, values: non-negative numbers that sum to 1 in each row
    rets_1day: pd.DataFrame, Columns: tickers, rows: dates, values: 1 day return: r_t = (S_t/S_{t-1})-1
    """
    # shift the weights forward one day, so they are known in time
    lagged_weights = portfolio_weights.shift(1).iloc[1:]
    begin_date = max(lagged_weights.index[0],  rets_1day.index[0])
    end_date   = min(lagged_weights.index[-1], rets_1day.index[-1])
    return (rets_1day.loc[begin_date:end_date] * lagged_weights[begin_date:end_date]).fillna(0.).sum(axis=1)

def turnover(exposures, lag):
    """
    exposures: pd.DataFrame. Columns: tickers, rows: dates, values: factor exposure
    """    
    turnovers = OrderedDict()
    
    for i in range(exposures.index.size-lag):
        current_exposures = exposures.iloc[lag + i].dropna()
        current_universe = current_exposures.index
        past_exposures = exposures.iloc[i][current_universe]

        turnovers[exposures.index[lag+i]] = scipy.stats.spearmanr(past_exposures, current_exposures, nan_policy='omit').correlation
    
    return pd.Series(turnovers)
