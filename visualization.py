import matplotlib.pyplot as plt
import scipy.stats
import numpy as np
import pandas as pd

def histogram_of_values(values, bins=50, ax=plt):
    mu, sigma = values.mean(), values.std()
    xmin = min(mu-3*sigma, values.min())
    xmax = max(mu+3*sigma, values.max())
    xs   = np.linspace(xmin, xmax, 300)
    ax.hist(values, bins=50, density=True)
    ax.plot(xs, scipy.stats.norm.pdf(xs, loc=mu, scale=sigma))

def readable_date_interval(interval):
    template = "{}-{}"
    return "{}/{}-{}/{}".format(interval.left.year, interval.left.month, interval.right.year, interval.right.month)

def pretty_resample_mean(df, bins):
    summary_df = df.groupby(pd.cut(df.index, bins)).mean()
    summary_df.index = [readable_date_interval(interval) for interval in summary_df.index]
    return summary_df
